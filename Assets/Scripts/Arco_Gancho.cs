﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arco_Gancho : MonoBehaviour
{
    public GameObject Gancho;
    public Player player;
    public bool facing;
    float horizontal;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontal = Input.GetAxis ("Horizontal");

        if(Input.GetMouseButtonDown(0)){
            Instantiate(Gancho,transform.position,Quaternion.identity,null);
        }
        if (horizontal > 0 && !facing) 
		{
			Flip ();
           
		}
		else if (horizontal < 0 && facing) 
		{
			Flip ();
            
		}
    }

    public void Flip(){
        
        facing =! facing;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    
    }
    
}
