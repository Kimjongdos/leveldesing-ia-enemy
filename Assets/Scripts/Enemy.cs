﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float VisionRadius;
    public float AttackRadius;
    public float Speed;
    private GameObject Player;
    Vector3 InitialPosition;
    private Rigidbody2D rb2d;
    public int JumpForce;
    //Atack
    public GameObject stone;
    private float counter;
    public float timetoshoot;
    public bool isFacingRight = true;
    public Weapon Weaaspon;
    private SpriteRenderer sprite;
    
    
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        InitialPosition = transform.position;  //Posicion Actual del enemigo
        rb2d = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
    IAEnemy();
    counter += Time.deltaTime;

    
            
    }

     private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(transform.position, VisionRadius);
        Gizmos.DrawWireSphere(transform.position, AttackRadius);
    }

    public void IAEnemy(){
        Vector3 target = InitialPosition;
        RaycastHit2D hit = Physics2D.Raycast(transform.position,Player.transform.position - transform.position,VisionRadius,1 << LayerMask.NameToLayer("Default"));

        Vector3 forward = transform.TransformDirection(Player.transform.position - transform.position);
        Debug.DrawRay(transform.position,forward,Color.red);

        if(hit.collider != null){
            if(hit.collider.tag == "Player"){
                target = Player.transform.position;
            }
        }   

        float distance = Vector3.Distance(target,transform.position);
        Vector3 dir = (target - transform.position).normalized;
        
        if(target != InitialPosition && distance < AttackRadius){
            if(counter>timetoshoot){
                Weaaspon.shoot();
            }
            
        }else
        {
            if(dir.x>0){
                rb2d.velocity = new Vector3( Speed * dir.x,0, 0);
                
            }else if(dir.x<0){
                rb2d.velocity = new Vector3( -Speed * -dir.x,0, 0);
                
                
            } 
           if(dir.x<0.1f){
               transform.localScale = new Vector3(-1,1,1);
           }else if(dir.x>0.1f){
               transform.localScale = new Vector3(1,1,1);
           }

        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Jump"){
            rb2d.AddForce(Vector2.up * JumpForce * Time.deltaTime);

         }
    }
       
   

   
}
