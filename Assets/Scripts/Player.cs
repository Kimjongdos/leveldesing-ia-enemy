﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	private Rigidbody2D myrigidbody;
	[SerializeField]
	private float movementspeed = 10f;
	[SerializeField]
	private float jumpheigt = 10f;
	private bool isGround;
	public bool facing = true;
	private float horizontal;
	public SpriteRenderer sprite;
	//Weapons
	public GameObject ArcoConFlechas;
	public GameObject ArcoConGancho;
	public bool WeaponFlecha;
    public bool WeaponGancho;
	
	public LayerMask whatIsGround;

	void Start ()
	{
		myrigidbody = GetComponent<Rigidbody2D> ();
	}
	
	void Update(){

		if(WeaponFlecha){
			ArcoConFlechas.SetActive(true);
			ArcoConGancho.SetActive(false);
		}else if(WeaponGancho){
			ArcoConGancho.SetActive(true);
			ArcoConFlechas.SetActive(false);
		}

	}
	void FixedUpdate ()
	{
		horizontal = Input.GetAxis ("Horizontal");
	
		myrigidbody.velocity = new Vector2 (horizontal * movementspeed , myrigidbody.velocity.y);
		if(Input.GetKeyDown(KeyCode.Space) && isGround )
		{
			myrigidbody.AddForce (Vector2.up * jumpheigt, ForceMode2D.Impulse);
			isGround = false;
			Debug.Log("He Saltado");
		}
		if (horizontal > 0 && !facing) 
		{
			Flip ();
		}
		else if (horizontal < 0 && facing) 
		{
			Flip ();
		}
	}
	private void Flip ()
	{
		facing = !facing;
		sprite.flipX = !facing;
	}
	
	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.tag == "Ground"){
			isGround = true;
		}
	}
}
