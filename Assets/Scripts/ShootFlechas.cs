﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFlechas : MonoBehaviour
{
    public GameObject crosshairs;
    private Vector3 target;
    public Camera cam;
    public GameObject ArcoPivote;
    public Player Jugador;
    public GameObject Flecha;
    public float bulletSpeed = 60f;
    public float TiempoDeDisparo;
    private float contador;
    Vector2 direccion;
    public GameObject ToInstantiate;
    float rotationZ;

    // Update is called once per frame
    void Start(){
        cam = Camera.main;
        Cursor.visible = false;
        contador = 0;
        
       
    }   
    void Update()   
    {
        Flechas();
               
    }

    public void Flechas(){
    target = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,transform.position.z)); // El target es igual a la posicion del mouse
    crosshairs.transform.position = new Vector2(target.x,target.y); // Aquí el puntero tendrá la posición del mouse

    Vector3 difference = target - ArcoPivote.transform.position; // Calculas la distancia del puntero y del ArcoPivote
    float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg; //Obtienes los angulos que hay entre el ArcoPivote y el puntero
    ArcoPivote.transform.rotation = Quaternion.Euler(0f,0f,rotationZ); // le asignas la rotacion al arco

       if(Input.GetMouseButton(1)){ 
           
           contador += Time.deltaTime;
           //float  limites = Mathf.Clamp(contador,0f,TiempoDeDisparo);
           Debug.Log(contador);
           float distance = difference.magnitude; // calcula la distancia entre el puntero y el ArcoPivote (?)
            direccion = difference / distance;
            
           
       }
        if(contador > TiempoDeDisparo && Input.GetMouseButtonDown(0)){
            DisparoFlecha(direccion,rotationZ);
            contador = 0f;
                     
        }
        if(Input.GetMouseButtonUp(1)){
            contador = 0f;
        }

    }

    

    void DisparoFlecha(Vector2 direccion,float rotationZ){
        GameObject b = Instantiate(Flecha) as GameObject; // nide de lo que es
        b.transform.position = ToInstantiate.transform.position; // le asignas X posicion al objeto
        b.transform.rotation = Quaternion.Euler(0,0,rotationZ); // le asignas la rotacionZ al gameObject b
        b.GetComponent<Rigidbody2D>().velocity = (direccion * bulletSpeed); //Le asignas velocidad al objeto prefab
    }

    
    
}

