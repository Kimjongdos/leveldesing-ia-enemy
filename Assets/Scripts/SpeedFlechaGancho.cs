﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlechaGancho : MonoBehaviour
{
    public float Speed;
    public Rigidbody2D rb2d;
    

    void Start()
    {
        rb2d.velocity = Vector2.right * Speed;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
}
