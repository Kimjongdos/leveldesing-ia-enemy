﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventarioManager : MonoBehaviour
{
    public GameObject Canvas;
    public Player player;
    void Start()
    {
        Canvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Q)){
            Canvas.SetActive(true);
            Cursor.visible = true;
            Time.timeScale = 0.4f;
        }else if(Input.GetKeyUp(KeyCode.Q)){
            Canvas.SetActive(false);
            Cursor.visible = false;    
            Time.timeScale = 1f;
        }
    }


    public void Flechas(){
        player.WeaponFlecha = true;
        player.WeaponGancho = false;
    }

    public void Gancho(){
       player.WeaponGancho = true;
       player.WeaponFlecha = false;
    }
}
