﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    DistanceJoint2D joint;
    Vector3 targetPos;
    RaycastHit2D hit;
    public float distance = 10f;
    public LayerMask mask;
    public LineRenderer line;
    public float step = 0.02f;
   
    void Start()
    {
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;
        line.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(joint.distance>1f){
            joint.distance -= step;

        }else{
            line.enabled = false;
            joint.enabled = false;
        }

        if(Input.GetKeyDown(KeyCode.Space)){
            
            targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;

            hit = Physics2D.Raycast(transform.position,targetPos - transform.position,distance,mask);

            if(hit.collider != null && hit.collider.gameObject.GetComponent<Rigidbody2D>()!=null){

                joint.enabled = true; //Activa el componente joint

                joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>(); //Obtiene el Rigidbody del objeto que el Raycast ha chocado y se lo pasa al CONECTEDJOINT

                joint.connectedAnchor = hit.point - new Vector2(hit.collider.transform.position.x,hit.collider.transform.position.y); //Centra el gancho en el objecto que esta colgado
                joint.distance = Vector2.Distance(transform.position,hit.point); //La distancia de la "LIALA"

                //Linea


                line.enabled = true;
                
               


                
            }
        }

        if(Input.GetKey(KeyCode.Space)){
            line.SetPosition(0,transform.position);
            line.SetPosition(1,hit.point);
        }

        if(Input.GetKeyUp(KeyCode.Space)){
            joint.enabled = false;
            line.enabled = false;
        }
    }
}
